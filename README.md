BA-CREATURES
============

About
-----

BA-CREATURES is a virtual ecosystem simulation written in the QBasic
programming language. It simulates three interacting artificial
lifeforms represented by colored circles: autotrophs (green circles),
herbivores (yellow circles), and carnivores (red circles).

All lifeforms wander about by default, decrease in size as they are
harmed, increase in size as they grow, and reproduce if they surpass
their maximum health. Autotrophs do not consume other lifeforms and will
simply grow if left unharmed; autotrophs flee from herbivores.
Herbivores will grow if they consume an autotroph and are harmed if they
are unable to consume an autotroph or are consumed by a carnivore;
herbivores chase autotrophs and flee from carnivores. Carnivores will
grow if they consume an herbivore and are harmed if they are unable to
consume an autotroph; carnivores chase herbivores.

Several configuration options may be set at the start:

* **Initial health**: Health of each creature at the start of the
                      simulation
* **Growth rate**: How quickly each creature increases in size
* **Maximum health**: Maximum health before creature reproduction occurs
* **Consumption factor**: Health applied to Creatures for consumption;
                          damage applied to prey when attacked
* **Metabolic rate**: Damage applied to Creature every cycle
* **Wander force**: Movement influence of creature's desire to wander
* **Flee force**: Movement influence of creature's desire to flee a
                  predator
* **Chase force**: Movement influence of creature's desire to chase a
                   prey
* **Maximum acceleration**: Maximum acceleration a creature can have
* **Maximum velocity**: Maximum velocity a creature can have
* **Flee sense**: Distance up to where creature can sense a predator
* **Chase sense**: Distance up to where creature can sense a prey
* **Population limit**: Maximum number of creatures at any given time during
                        the simulation
* **Autotrophs**: Number of autotrophs at the start of the simulation
* **Herbivores**: Number of herbivores at the start of the simulation
* **Carnivores**: Number of carnivores at the start of the simulation

Pressing the ESC key will end the simulation.

History
-------

I originally uploaded my initial attempt to create BA-CREATURES to
[The QBasic Station](https://web.archive.org/web/20070811060322/http://www.qbasicstation.com/index.php?c=p_member&filecat=1)
on 23 April 2007.

> I originally got this idea after reading an old book on Java
> Programming. I worked with the idea of having virtual pets, and then
> it spawned into a virtual ecosystem idea. So now here's BA-CREATURES.
> Right now it's just 5 circles, "creatures," moving around on the
> screen, but I plan on updating this often.

However, because I had only started programming about a year before, I
was unable to complete the project to the extent I had envisioned. Now
that I have significantly more experience in software development, I
decided to complete the BA-CREATURES project I wanted to complete all
those years earlier.

Licensing
---------

BA-CREATURES is released under the BSD Zero Clause License.

See the file LICENSE for copying conditions.
