' SPDX-License-Identifier: 0BSD
' Copyright (C) 2021 William Breathitt Gray

RANDOMIZE TIMER

TYPE Position
	x AS DOUBLE
	y AS DOUBLE
END TYPE

TYPE Creature
	center AS Position
	vel AS Position
	chase AS Position
	flee AS Position
	health AS DOUBLE
	radius AS DOUBLE
	feeding AS INTEGER
END TYPE

CONST SCREENWIDTH = 640
CONST SCREENHEIGHT = 350
CONST MAXCREATURES = 799
CONST AUTOTROPH = 10
CONST HERBIVORE = 14
CONST CARNIVORE = 12

DIM SHARED creatures(1 TO MAXCREATURES) AS Creature
DIM SHARED populationLimit AS INTEGER
DIM SHARED numCreatures AS INTEGER
DIM SHARED initialHealth AS DOUBLE
DIM SHARED growthRate AS DOUBLE
DIM SHARED initialRadius AS DOUBLE
DIM SHARED maximumRadius AS DOUBLE
DIM SHARED maximumHealth AS DOUBLE
DIM SHARED consumeFactor AS DOUBLE
DIM SHARED metabolicRate AS DOUBLE
DIM SHARED wanderForce AS DOUBLE
DIM SHARED fleeForce AS DOUBLE
DIM SHARED chaseForce AS DOUBLE
DIM SHARED maximumAcceleration AS DOUBLE
DIM SHARED maximumVelocity AS DOUBLE
DIM SHARED fleeSense AS DOUBLE
DIM SHARED chaseSense AS DOUBLE
populationLimit = 0
numCreatures = 0

CALL initializeSimulation
CALL runSimulation

SUB initializeCreatures (creatureType AS INTEGER)
	DIM initial AS INTEGER
	DIM remaining AS INTEGER
	DIM index AS INTEGER
	DIM i AS INTEGER
	remaining = populationLimit - numCreatures

	DO
		SELECT CASE creatureType
			CASE AUTOTROPH
				INPUT "Autotrophs: ", initial
			CASE HERBIVORE
				INPUT "Herbivores: ", initial
			CASE CARNIVORE
				INPUT "Carnivores: ", initial
		END SELECT

		IF initial < 0 OR initial > remaining THEN
			PRINT "Please enter a number between 0 and "; remaining
		ELSE
			EXIT DO
		END IF
	LOOP

	FOR i = 1 TO initial
		IF numCreatures > 0 THEN
			' Insert creature at random location in the population
			index = RND * numCreatures + 1
			creatures(numCreatures + 1) = creatures(index)
		ELSE
			index = 1
		END IF

		' Initialize creature
		creatures(index).center.x = RND * (SCREENWIDTH - creatures(index).radius * 2) + creatures(index).radius
		creatures(index).center.y = RND * (SCREENHEIGHT - creatures(index).radius * 2) + creatures(index).radius
		creatures(index).health = initialHealth
		creatures(index).radius = initialRadius
		creatures(index).feeding = creatureType
		creatures(index).vel.x = 0
		creatures(index).vel.y = 0
		creatures(index).chase.x = 0
		creatures(index).chase.y = 0
		creatures(index).flee.x = 0
		creatures(index).flee.y = 0

		numCreatures = numCreatures + 1
	NEXT
END SUB

SUB initializeSimulation
	DO
		INPUT "Initial health: ", initialHealth
		IF initialHealth <= 0 THEN
			PRINT "Please enter a value greater than 0."
		ELSE
			EXIT DO
		END IF
	LOOP
	DO
		INPUT "Growth rate: ", growthRate
		IF growthRate <= 0 THEN
			PRINT "Please enter a value greater than 0."
		ELSE
			EXIT DO
		END IF
	LOOP
	initialRadius = FIX(initialHealth / growthRate) + 1
	DO
		INPUT "Maximum health: ", maximumHealth
		IF maximumHealth <= initialHealth THEN
			PRINT "Please enter a value greater than "; initialHealth
		ELSE
			EXIT DO
		END IF
	LOOP
	maximumRadius = FIX(maximumHealth / growthRate) + 1
	INPUT "Consumption factor: ", consumeFactor
	INPUT "Metabolic rate: ", metabolicRate
	INPUT "Wander force: ", wanderForce
	INPUT "Flee force: ", fleeForce
	INPUT "Chase force: ", chaseForce
	INPUT "Maximum acceleration: ", maximumAcceleration
	INPUT "Maximum velocity: ", maximumVelocity
	DO
		INPUT "Flee sense: ", fleeSense
		IF fleeSense < 0 THEN
			PRINT "Please enter a nonnegative value."
		ELSE
			EXIT DO
		END IF
	LOOP
	DO
		INPUT "Chase sense: ", chaseSense
		IF chaseSense < 0 THEN
			PRINT "Please enter a nonnegative value."
		ELSE
			EXIT DO
		END IF
	LOOP
	DO
		INPUT "Population limit: ", populationLimit
		IF populationLimit < 0 OR populationLimit > MAXCREATURES THEN
			PRINT "Please enter a number between 0 and "; MAXCREATURES
		ELSE
			EXIT DO
		END IF
	LOOP
	CALL initializeCreatures(AUTOTROPH)
	CALL initializeCreatures(HERBIVORE)
	CALL initializeCreatures(CARNIVORE)
END SUB

SUB drawSimulation (aPage AS INTEGER, vPage AS INTEGER)
	DIM i AS INTEGER

	SCREEN 9, , aPage, vPage

	CLS

	FOR i = 1 TO numCreatures
		CIRCLE (creatures(i).center.x, creatures(i).center.y), creatures(i).radius, creatures(i).feeding
	NEXT
END SUB

SUB randomDirection (direction AS Position)
	DIM r AS SINGLE
	' QBasic uses a linear congruential generator with a modulo 2^24
	CONST M = &H1000000

	' RND has range [0,1), so first normalize it to [0,1]
	r = RND * M / (M - 1)

	' Leverage equation for unit circle to get point
	direction.x = r * 2 - 1
	direction.y = SQR(1 - direction.x * direction.x)
	IF RND < 0.5 THEN
		direction.y = -direction.y
	END IF
END SUB

SUB addForce (force AS Position, magnitude AS DOUBLE, direction AS Position)
	force.x = force.x + magnitude * direction.x
	force.y = force.y + magnitude * direction.y
END SUB

SUB senseCreature (acceleration AS Position, force AS DOUBLE, distance AS DOUBLE, direction AS Position)
	DIM intensity AS DOUBLE

	IF creatureEdge > 0 THEN
		intensity = force / (distance * distance)
	ELSE
		intensity = maximumAcceleration
	END IF

	CALL addForce(acceleration, intensity, direction)
END SUB

SUB cornerPrey (acceleration AS Position, predator AS Creature, prey AS Creature)
	DIM senseRadius AS DOUBLE
	DIM distance AS DOUBLE
	DIM force AS DOUBLE

	senseRadius = predator.radius + chaseSense
	IF acceleration.x < 0 AND predator.center.x - senseRadius < 0 THEN
		distance = (prey.center.x - prey.radius) + 1
		force = -acceleration.x * chaseForce / (distance * distance)
		acceleration.x = acceleration.x - force
	ELSEIF acceleration.x > 0 AND predator.center.x + senseRadius > SCREENWIDTH - 1 THEN
		distance = SCREENWIDTH - (prey.center.x + prey.radius)
		force = acceleration.x * chaseForce / (distance * distance)
		acceleration.x = acceleration.x + force
	END IF
	IF acceleration.y < 0 AND predator.center.y - senseRadius < 0 THEN
		distance = (prey.center.y - prey.radius) + 1
		force = -acceleration.y * chaseForce / (distance * distance)
		acceleration.y = acceleration.y - force
	ELSEIF acceleration.y > 0 AND predator.center.y + senseRadius > SCREENHEIGHT - 1 THEN
		distance = SCREENHEIGHT - (prey.center.y + prey.radius)
		force = acceleration.y * chaseForce / (distance * distance)
		acceleration.y = acceleration.y + force
	END IF
END SUB

SUB handlePredation (prey AS Creature, predator AS Creature, deltaTime AS DOUBLE)
	DIM offset AS Position
	DIM threshold AS DOUBLE
	DIM distance AS DOUBLE
	DIM distanceSquared AS DOUBLE
	DIM predatorTier AS DOUBLE
	DIM preyTier AS DOUBLE
	DIM consumption AS DOUBLE
	DIM creatureEdge AS DOUBLE
	DIM chase AS Position

	offset.x = prey.center.x - predator.center.x
	offset.y = prey.center.y - predator.center.y
	distanceSquared = offset.x * offset.x + offset.y * offset.y

	' Ensure creatures are close enough to interact
	threshold = prey.radius + predator.radius
	distance = threshold + fleeSense + chaseSense
	IF distanceSquared > distance * distance THEN
		EXIT SUB
	END IF

	distance = SQR(distanceSquared)
	predatorTier = predator.radius / maximumRadius
	preyTier = prey.radius / maximumRadius

	' Check for collision
	IF distance <= threshold THEN
		consumption = consumeFactor * deltaTime * predatorTier

		' Handle prey death
		IF prey.health <= consumption THEN
			predator.health = predator.health + prey.health
			prey.health = 0
			EXIT SUB
		END IF

		prey.health = prey.health - consumption
		predator.health = predator.health + consumption
	END IF

	IF distance = 0 THEN
		' Prey tries to escape in any direction
		CALL randomDirection(offset)
		CALL addForce(prey.flee, fleeForce * predatorTier, offset)
		EXIT SUB
	END IF

	' Normalize offset vector pointing toward the prey
	offset.x = offset.x/distance
	offset.y = offset.y/distance

	' Chase prey
	creatureEdge = distance - prey.radius
	IF predator.radius + chaseSense >= creatureEdge THEN
		CALL senseCreature(chase, chaseForce * preyTier, creatureEdge, offset)
		CALL cornerPrey(chase, predator, prey)
		predator.chase.x = predator.chase.x + chase.x
		predator.chase.y = predator.chase.y + chase.y
	END IF

	' Flee predator
	creatureEdge = distance - predator.radius
	IF prey.radius + fleeSense >= creatureEdge THEN
		CALL senseCreature(prey.flee, fleeForce * predatorTier, creatureEdge, offset)
	END IF
END SUB

SUB interactCreatures (prime AS Creature, contingent AS Creature, deltaTime AS DOUBLE)
	' Skip dead creature
	IF contingent.health <= 0 THEN
		EXIT SUB
	END IF

	SELECT CASE prime.feeding
		CASE AUTOTROPH
			' Skip neutral creatures
			IF contingent.feeding <> HERBIVORE THEN
				EXIT SUB
			END IF
			CALL handlePredation(prime, contingent, deltaTime)
		CASE HERBIVORE
			SELECT CASE contingent.feeding
				CASE AUTOTROPH
					CALL handlePredation(contingent, prime, deltaTime)
				CASE HERBIVORE
					' Skip neutral creatures
					EXIT SUB
				CASE CARNIVORE
					CALL handlePredation(prime, contingent, deltaTime)
			END SELECT
		CASE CARNIVORE
			' Skip neutral creatures
			IF contingent.feeding <> HERBIVORE THEN
				EXIT SUB
			END IF
			CALL handlePredation(contingent, prime, deltaTime)
	END SELECT
END SUB

SUB avoidBoundaries (c AS Creature)
	DIM senseRadius AS DOUBLE
	DIM distance AS DOUBLE
	DIM force AS DOUBLE

	senseRadius = c.radius + fleeSense
	IF c.flee.x < 0 AND c.center.x - senseRadius < 0 THEN
		distance = (c.center.x - c.radius) + 1
		force = -c.flee.x * fleeForce / (distance * distance)
		c.flee.x = c.flee.x + force
	ELSEIF c.flee.x > 0 AND c.center.x + senseRadius > SCREENWIDTH - 1 THEN
		distance = SCREENWIDTH - (c.center.x + c.radius)
		force = c.flee.x * fleeForce / (distance * distance)
		c.flee.x = c.flee.x - force
	END IF
	IF c.flee.y < 0 AND c.center.y - senseRadius < 0 THEN
		distance = (c.center.y - c.radius) + 1
		force = -c.flee.y * fleeForce / (distance * distance)
		c.flee.y = c.flee.y + force
	ELSEIF c.flee.y > 0 AND c.center.y + senseRadius > SCREENHEIGHT - 1 THEN
		distance = SCREENHEIGHT - (c.center.y + c.radius)
		force = c.flee.y * fleeForce / (distance * distance)
		c.flee.y = c.flee.y - force
	END IF
END SUB

SUB limitVector (vector AS Position, maximum AS DOUBLE)
	DIM magnitude AS DOUBLE

	magnitude = SQR(vector.x * vector.x + vector.y * vector.y)
	IF magnitude > maximum THEN
		vector.x = vector.x / magnitude
		vector.y = vector.y / magnitude
		vector.x = vector.x * maximum
		vector.y = vector.y * maximum
	END IF
END SUB

SUB moveCreature (c AS Creature, deltaTime AS DOUBLE)
	DIM acceleration AS Position
	DIM direction AS Position

	acceleration.x = c.chase.x + c.flee.x
	acceleration.y = c.chase.y + c.flee.y
	c.chase.x = 0
	c.chase.y = 0
	c.flee.x = 0
	c.flee.y = 0

	' Handle wander force
	CALL randomDirection(direction)
	CALL addForce(acceleration, wanderForce, direction)

	CALL limitVector(acceleration, maximumAcceleration)
	c.vel.x = c.vel.x + acceleration.x * deltaTime
	c.vel.y = c.vel.y + acceleration.y * deltaTime

	CALL limitVector(c.vel, maximumVelocity)
	c.center.x = c.center.x + c.vel.x * deltaTime
	c.center.y = c.center.y + c.vel.y * deltaTime

	IF c.center.x - c.radius < 0 THEN
		c.vel.x = 0
		c.center.x = c.radius
	ELSEIF c.center.x + c.radius > SCREENWIDTH - 1 THEN
		c.vel.x = 0
		c.center.x = SCREENWIDTH - 1 - c.radius
	END IF
	IF c.center.y - c.radius < 0 THEN
		c.vel.y = 0
		c.center.y = c.radius
	ELSEIF c.center.y + c.radius > SCREENHEIGHT - 1 THEN
		c.vel.y = 0
		c.center.y = SCREENHEIGHT - 1 - c.radius
	END IF
END SUB

SUB tickSimulation (deltaTime AS DOUBLE)
	DIM i AS INTEGER
	DIM j AS INTEGER
	DIM creatureTier AS DOUBLE

	FOR i = numCreatures TO 1 STEP -1
		' Handle interactions
		FOR j = i - 1 TO 1 STEP -1
			' Exit if dead
			IF creatures(i).health <= 0 THEN
				EXIT FOR
			END IF
			CALL interactCreatures(creatures(i), creatures(j), deltaTime)
		NEXT

		' Handle metabolic expense
		creatureTier = creatures(i).radius / maximumRadius
		creatures(i).health = creatures(i).health - metabolicRate * deltaTime * creatureTier

		' Reap dead creature
		IF creatures(i).health <= 0 THEN
			' Defragment creature list
			IF i <> numCreatures THEN
				creatures(i) = creatures(numCreatures)
			END IF
			numCreatures = numCreatures - 1
			GOTO finishCreature
		END IF

		CALL avoidBoundaries(creatures(i))

		' Handle autotroph feeding
		IF creatures(i).feeding = AUTOTROPH THEN
			creatures(i).health = creatures(i).health + consumeFactor * deltaTime * creatureTier
		END IF

		' Handle reproduction and growth
		IF creatures(i).health > maximumHealth THEN
			IF numCreatures = populationLimit THEN
				creatures(i).health = maximumHealth
			ELSE
				creatures(i).health = maximumHealth / 2
				numCreatures = numCreatures + 1
				creatures(numCreatures) = creatures(i)
			END IF
		END IF
		creatures(i).radius = FIX(creatures(i).health / growthRate) + 1

		CALL moveCreature(creatures(i), deltaTime)
finishCreature:
	NEXT
END SUB

SUB runSimulation
	DIM K AS STRING
	DIM currentTime AS DOUBLE
	DIM lastTime AS DOUBLE
	DIM aPage AS INTEGER
	DIM vPage AS INTEGER
	currentTime = TIMER
	lastTime = currentTime
	aPage = 1
	vPage = 0

	DO
		DO
			K = INKEY$
			IF K = CHR$(27) THEN END
		LOOP UNTIL K = ""

		CALL drawSimulation(aPage, vPage)
		SWAP aPage, vPage

		CALL tickSimulation(currentTime - lastTime)

		lastTime = currentTime

		' Ensure we have a new clock tick before the next cycle
		DO
			currentTime = TIMER
		LOOP WHILE currentTime = lastTime
		' Adjust for midnight rollover
		IF lastTime > currentTime THEN
			lastTime = lastTime - 86400
		END IF
	LOOP
END SUB
